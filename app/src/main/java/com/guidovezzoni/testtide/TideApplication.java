package com.guidovezzoni.testtide;

import android.app.Application;

import com.karumi.dexter.Dexter;

/**
 * Created by guido on 13/12/16.
 * This is needed only by Dexter
 */
public class TideApplication extends Application {
    @Override public void onCreate() {
        super.onCreate();
        Dexter.initialize(this);
    }
}
