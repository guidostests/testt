package com.guidovezzoni.testtide.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.guidovezzoni.testtide.mvp.ListFragment;
import com.guidovezzoni.testtide.mvp.MapFragment;

/**
 * Created by guido on 13/12/16.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    private int mNoFragment;

    public PagerAdapter(FragmentManager fm, int noFragment) {
        super(fm);
        mNoFragment = noFragment;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListFragment();
            case 1:
                return new MapFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNoFragment;
    }
}
