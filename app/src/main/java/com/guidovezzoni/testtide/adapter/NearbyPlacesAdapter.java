package com.guidovezzoni.testtide.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.guidovezzoni.testtide.R;
import com.guidovezzoni.testtide.model.api.Location;
import com.guidovezzoni.testtide.model.api.Result;
import com.guidovezzoni.testtide.viewholder.NearbyPlacesViewHolder;

import java.util.List;

public class NearbyPlacesAdapter extends RecyclerView.Adapter<NearbyPlacesViewHolder> {
    private List<Result> mList;
    private Context mContext;
    private AdapterListener mListener;

    public NearbyPlacesAdapter(Context context, List<Result> list, AdapterListener listener) {
        mContext = context;
        mList = list;
        mListener = listener;
    }

    @Override
    public NearbyPlacesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_nearby_places, parent, false);
        return new NearbyPlacesViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NearbyPlacesViewHolder holder, int position) {
        final Result result = mList.get(position);

        holder.mName.setText(result.name != null ? result.name : "");
        holder.mVicinity.setText(result.vicinity != null ? result.vicinity : "");
        holder.mDistance.setText(result.distanceFromCurrentLocation != null ?
                mContext.getString(R.string.format_distance, result.distanceFromCurrentLocation)
                : mContext.getString(R.string.format_distance_na));

        Glide.with(mContext)
                .load(result.icon)
                .centerCrop()
                .placeholder(R.drawable.ic_cached_black_24dp)
                .into(holder.mMamIconnImage);

        holder.mContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener!=null) {
                    mListener.onItemClicked(result.geometry.location, result.name);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface AdapterListener{
        void onItemClicked(Location location, String address);
    }
}
