
package com.guidovezzoni.testtide.model.api;

import com.google.gson.annotations.Expose;

import java.util.List;

public class Result {

    public Geometry geometry;
    public String icon;
    public String id;
    public String name;
    public OpeningHours openingHours;
    public List<Photo> photos = null;
    public String placeId;
    public Double rating;
    public String reference;
    public String scope;
    public List<String> types = null;
    public String vicinity;

    /**
     * I've added this field for simplicity, but it isn't the right approach
     */
    @Expose(serialize = false, deserialize = false)
    public Float distanceFromCurrentLocation;

}
