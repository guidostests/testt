
package com.guidovezzoni.testtide.model.api;

import java.util.List;

public class NearbyPlacesResponse {

    public List<Object> htmlAttributions = null;
    public String nextPageToken;
    public List<Result> results = null;
    public String status;

}
