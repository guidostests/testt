
package com.guidovezzoni.testtide.model.api;

import java.util.List;

public class Photo {

    public Integer height;
    public List<String> htmlAttributions = null;
    public String photoReference;
    public Integer width;

}
