package com.guidovezzoni.testtide.model.event;

import com.guidovezzoni.testtide.model.api.NearbyPlacesResponse;

/**
 * Created by guido on 14/12/16.
 */

public class NearbyPlacesReceivedEvent {
    public NearbyPlacesResponse mNearbyPlacesResponse;

    public NearbyPlacesReceivedEvent(NearbyPlacesResponse nearbyPlacesResponse) {
        mNearbyPlacesResponse = nearbyPlacesResponse;
    }
}
