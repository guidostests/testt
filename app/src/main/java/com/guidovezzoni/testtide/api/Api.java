package com.guidovezzoni.testtide.api;

/**
 * Created by guido on 14/12/16.
 */

public class Api {
    // query params
    public static final String QUERY_PARAM_LOCATION = "location";
    public static final String QUERY_PARAM_RADIUS = "radius";
    public static final String QUERY_PARAM_TYPE = "type";
    public static final String QUERY_PARAM_KEY = "key";


//    https://maps.googleapis.com/maps/api/place/nearbysearch/json
//            ?location=51.42270505428314,-0.05295753479003906
//            &radius=10000
//            &type=restaurant
//            &key=AIzaSyBE66acGO4FZDM0LxmHm4HIZZd2LsI7G1g

    // root folders
    public static final String MAPS = "maps";
    public static final String API = "api";
    public static final String PLACE = "place";
    public static final String NEARBYSEARCH = "nearbysearch";
    public static final String JSON = "json";

    // full endpoints
    public static final String NEARBY_SEARCH = MAPS + "/" + API + "/" + PLACE+ "/" + NEARBYSEARCH+ "/" + JSON;

}
