package com.guidovezzoni.testtide.api;

import com.guidovezzoni.testtide.model.api.NearbyPlacesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by guido on 14/12/16.
 */

public interface GoogleApiNearbyServices {
    /*
    https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=51.42270505428314,-0.05295753479003906&radius=10000&type=restaurant&sensor=true&key=AIzaSyBE66acGO4FZDM0LxmHm4HIZZd2LsI7G1g

    https://maps.googleapis.com/maps/api/place/nearbysearch/json
    ?location=51.42270505428314,-0.05295753479003906
    &radius=10000
    &type=restaurant
    &key=AIzaSyBE66acGO4FZDM0LxmHm4HIZZd2LsI7G1g
     */

    @GET(Api.NEARBY_SEARCH)
    Call<NearbyPlacesResponse> getNearbySearch(@Query(Api.QUERY_PARAM_LOCATION) String location,
                                               @Query(Api.QUERY_PARAM_RADIUS) long radius,
                                               @Query(Api.QUERY_PARAM_TYPE) String type,
                                               @Query(Api.QUERY_PARAM_KEY) String key);
}
