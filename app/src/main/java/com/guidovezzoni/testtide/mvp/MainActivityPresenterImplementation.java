package com.guidovezzoni.testtide.mvp;

import android.location.Location;

import com.guidovezzoni.mvp.BasePresenterImplementation;
import com.guidovezzoni.mvp.ExtendedContract;
import com.guidovezzoni.testtide.model.api.NearbyPlacesResponse;
import com.guidovezzoni.testtide.model.api.Result;

/**
 * Created by guido on 14/12/16.
 */

public class MainActivityPresenterImplementation
        extends BasePresenterImplementation<MainActivityContract.View, MainActivityContract.Model>
        implements MainActivityContract.Presenter {

    private Location mLastLocation;
    private String mKey;

    public MainActivityPresenterImplementation(String key) {
        super();
        mKey = key;
    }

    @Override
    public void viewNeedsData() {
        requestData();
    }

    @Override
    public MainActivityContract.Model initModel() {
        return new MainActivityModelImplementation(mKey);
    }

    @Override
    public void cancelAsyncRequests() {
    }

    /**
     * get data from the model and forward it to the view when ready
     */
    private void requestData() {
        getModel().retrieveData(mLastLocation, new ExtendedContract.Model.OnModelListener<NearbyPlacesResponse>() {
            @Override
            public void onDataRetrieved(NearbyPlacesResponse data) {
                if (getView() != null) {

                    // calculate and update distance from current location....
                    // although it'd make sense it was provide from the server
                    for (Result result:data.results) {

                        Location location = new Location("");
                        location.setLatitude(result.geometry.location.lat);
                        location.setLongitude(result.geometry.location.lng);

                        result.distanceFromCurrentLocation = mLastLocation.distanceTo(location);
                    }

                    getView().dataRequestSuccessful(data);
                }
            }

            @Override
            public void onDataUnavailable(String message) {
                if (getView() != null) {
                    getView().dataRequestError(message);
                }
            }

            @Override
            public void onRequestCancelled() {
                if (getView() != null) {
                    getView().dataRequestCancelled();
                }
            }
        });
    }

    @Override
    public void updateLastKnownLocation(Location location) {
        mLastLocation = location;
    }
}
