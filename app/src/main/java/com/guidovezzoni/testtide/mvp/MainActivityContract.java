package com.guidovezzoni.testtide.mvp;

import android.location.Location;

import com.guidovezzoni.mvp.ExtendedContract;
import com.guidovezzoni.testtide.model.api.NearbyPlacesResponse;

/**
 * Created by guido on 14/12/16.
 */

public interface MainActivityContract extends ExtendedContract<NearbyPlacesResponse, Location,
        MainActivityContract.View, MainActivityContract.Presenter, MainActivityContract.Model> {

    interface View extends ExtendedContract.View<NearbyPlacesResponse, Presenter> {

    }

    interface Presenter extends ExtendedContract.Presenter<View, Model> {

        void updateLastKnownLocation(Location location);
    }

    interface Model extends ExtendedContract.Model<Location, Presenter> {

    }

}
