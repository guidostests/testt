package com.guidovezzoni.testtide.mvp;

import android.location.Location;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.guidovezzoni.mvp.BaseModelImplementation;
import com.guidovezzoni.testtide.api.GoogleApiNearbyServices;
import com.guidovezzoni.testtide.model.api.NearbyPlacesResponse;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by guido on 14/12/16.
 */

public class MainActivityModelImplementation
        extends BaseModelImplementation<MainActivityContract.Presenter>
        implements MainActivityContract.Model {

    private static final String BASE_URL = "https://maps.googleapis.com/";
    private static final String SERVICE_ERROR = "Service error";
    private static final String PLACE_TYPE = "bar";
    private static final long PLACER_RADIUS = 1000L;


    private GoogleApiNearbyServices mGoogleApiNearbyServices;
    private Call<NearbyPlacesResponse> mPlacesCall;

    private String mKey;


    public MainActivityModelImplementation(String key) {
        super();
        mKey = key;
    }

    @Override
    public void attachPresenter(MainActivityContract.Presenter basePresenter) {
        super.attachPresenter(basePresenter);

        // intialise retrofit service
        mGoogleApiNearbyServices = getGoogleApiNearbyServices();
    }


    /**
     * start network request and handle retrofit callbacks
     *
     */
    @Override
    public void retrieveData(final Location location, @NonNull final OnModelListener onModelListener) {
        if (onModelListener == null) {
            throw new NullPointerException("onModelListener");
        }

        //TODO: result is paginated so if required we should iterate through results before returning
        retrieveDataOnePage(location, onModelListener);
    }

    private void retrieveDataOnePage(final Location location, @NonNull final OnModelListener onModelListener) {
        String locationString=String.format(" %.8f,%.8f", location.getLatitude(), location.getLongitude());
        mPlacesCall = mGoogleApiNearbyServices.getNearbySearch(locationString, PLACER_RADIUS, PLACE_TYPE, mKey);
        mPlacesCall.enqueue(new Callback<NearbyPlacesResponse>() {
            @Override
            public void onResponse(Call<NearbyPlacesResponse> call, Response<NearbyPlacesResponse> response) {
                if (response.isSuccessful()) {
                    onModelListener.onDataRetrieved(response.body());
                } else {

                    // TODO this could be more sophisticated....
                    String errorMsg;
                    try {
                        errorMsg = response.errorBody().string();
                    } catch (IOException e) {
                        errorMsg = SERVICE_ERROR;
                    }
                    onModelListener.onDataUnavailable(errorMsg);
                }
            }

            @Override
            public void onFailure(Call<NearbyPlacesResponse> call, Throwable t) {
                if (call.isCanceled()) {
                    // this case will occur when retrofit request gets cancelled
                    onModelListener.onRequestCancelled();
                } else {
                    onModelListener.onDataUnavailable(t.getMessage());
                }
            }
        });
    }

    /**
     * build retrofit service with additional code to handle timestamps and logs
     *
     * @return
     */
    private GoogleApiNearbyServices getGoogleApiNearbyServices() {

        // handle logs
        OkHttpClient okHttpClient = new OkHttpClient
                .Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        // handle timestamps
        Gson gson = new GsonBuilder()
//                .setDateFormat("yyyy-MM-dd' 'HH:mm:ss")
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        return retrofit.create(GoogleApiNearbyServices.class);
    }

    @Override
    public void cancelAsyncRequests() {
        if (mPlacesCall!=null) {
            mPlacesCall.cancel();
        }
    }


}
