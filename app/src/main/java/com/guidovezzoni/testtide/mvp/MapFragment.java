package com.guidovezzoni.testtide.mvp;


import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.guidovezzoni.testtide.R;
import com.guidovezzoni.testtide.fragment.BasePagerFragment;
import com.guidovezzoni.testtide.model.api.Result;
import com.guidovezzoni.testtide.model.event.NearbyPlacesReceivedEvent;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.greenrobot.eventbus.Subscribe;

/**
 * GoogleApiClient and LocationRequest shoudl probably go into the model, but because of the need of
 * Context I decied to leave them here... but the other option is viable too
 */
public class MapFragment extends BasePagerFragment {

    private static final int MAP_ZOOM_FACTOR = 14;
    private static final long MAP_INTERVAL = 60000;
    private static final long MAP_DISPLACEMENT = 20;

    private GoogleMap mGoogleMap;
    private MapView mMapView;

    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;


    /**
     * this is here just for readability
     */
    private OnMapReadyCallback mOnMapReadyCallback = new OnMapReadyCallback() {
        @Override
        public void onMapReady(GoogleMap mMap) {
            mGoogleMap = mMap;

            //nice lib to handle permissions
            Dexter.checkPermission(new PermissionListener() {
                @Override
                public void onPermissionGranted(PermissionGrantedResponse response) {
                    setupGoogleApiClient();

                    mGoogleMap.setMyLocationEnabled(true);
                }

                @Override
                public void onPermissionDenied(PermissionDeniedResponse response) {
                    Toast.makeText(getActivity(), "Location needed in order to continue", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {

                }
            }, Manifest.permission.ACCESS_FINE_LOCATION);

        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.getMapAsync(mOnMapReadyCallback);


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    private synchronized void setupGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        setupLocationRequest();
                    }

                    @Override
                    public void onConnectionSuspended(int i) {

                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                    }
                })
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)

                .build();
        mGoogleApiClient.connect();
    }


    private void setupLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(MAP_INTERVAL);
        mLocationRequest.setSmallestDisplacement(MAP_DISPLACEMENT);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mListener.onUpdateCurrentLocation(location);

                    mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                            new LatLng(location.getLatitude(), location.getLongitude()), MAP_ZOOM_FACTOR));

                    mListener.onReadyToShowPlaces();

                    //stop location updates
                    if (mGoogleApiClient != null) {
                        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                    }
                }
            });
        }

    }

    /**
     * When the presenter sends info to teh activity, it will forward the data to fragment via eventbus
     *
     * @param nearbyPlacesreceivedEvent
     */
    @Subscribe
    public void onMessageEvent(NearbyPlacesReceivedEvent nearbyPlacesreceivedEvent) {
        mGoogleMap.clear();

        // TODO a more sophisticated way to chose viewport and zoom should be implemented, that
        // accounts for all results

        for (Result result : nearbyPlacesreceivedEvent.mNearbyPlacesResponse.results) {

            MarkerOptions markerOptions = new MarkerOptions();

            LatLng latLng = new LatLng(result.geometry.location.lat, result.geometry.location.lng);

            Location location = new Location("");
            location.setLatitude(result.geometry.location.lat);
            location.setLongitude(result.geometry.location.lng);

            markerOptions.position(latLng);
            markerOptions.title(result.name + " : Distance: " +
                    String.format(" %.0f", result.distanceFromCurrentLocation) + " meters");

            mGoogleMap.addMarker(markerOptions);
        }
    }
}
