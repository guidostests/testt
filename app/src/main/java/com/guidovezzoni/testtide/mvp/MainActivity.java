package com.guidovezzoni.testtide.mvp;

import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.guidovezzoni.testtide.R;
import com.guidovezzoni.testtide.adapter.PagerAdapter;
import com.guidovezzoni.testtide.fragment.BasePagerFragment;
import com.guidovezzoni.testtide.model.api.NearbyPlacesResponse;
import com.guidovezzoni.testtide.model.event.NearbyPlacesReceivedEvent;

import org.greenrobot.eventbus.EventBus;

public class MainActivity
        extends AppCompatActivity
        implements MainActivityContract.View, BasePagerFragment.OnFragmentListener {

    private MainActivityContract.Presenter mPresenter;

    private Toolbar mToolbar;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;

    @Override
    public MainActivityContract.Presenter getPresenter() {
        return mPresenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_tide);

//        Dexter.continuePendingRequestsIfPossible(permissionsListener);

        mPresenter = new MainActivityPresenterImplementation(getString(R.string.google_maps_key));

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mTabLayout.addTab(mTabLayout.newTab().setText("List"));
        mTabLayout.addTab(mTabLayout.newTab().setText("Map"));
        mTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        mViewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), mTabLayout.getTabCount());
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.detachView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.attachView(this);
    }

    /**
     * This method is called from presenter when data to show is available
     * It forwards the info to the fragments
     *
     * @param data downloaded data
     */
    @Override
    public void dataRequestSuccessful(NearbyPlacesResponse data) {
        EventBus.getDefault().post(new NearbyPlacesReceivedEvent(data));
    }

    /**
     * called from presenter when an error happens
     * @param message
     */
    @Override
    public void dataRequestError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    /**
     * network request was cancelled, f.i. due to a config change
     */
    @Override
    public void dataRequestCancelled() {

    }

    @Override
    public void onReadyToShowPlaces() {
        mPresenter.viewNeedsData();
    }

    /**
     * Location is comunicated from the fragment
     * @param location
     */
    @Override
    public void onUpdateCurrentLocation(Location location) {
        mPresenter.updateLastKnownLocation(location);
    }
}
