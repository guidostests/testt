package com.guidovezzoni.testtide.mvp;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.guidovezzoni.testtide.R;
import com.guidovezzoni.testtide.adapter.NearbyPlacesAdapter;
import com.guidovezzoni.testtide.fragment.BasePagerFragment;
import com.guidovezzoni.testtide.model.api.Location;
import com.guidovezzoni.testtide.model.event.NearbyPlacesReceivedEvent;

import org.greenrobot.eventbus.Subscribe;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends BasePagerFragment {

    private RecyclerView mRecyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_list, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.list_recyclerView);


        return rootView;
    }

    /**
     * Sent by the activity
     *
     * @param nearbyPlacesreceivedEvent
     */
    @Subscribe
    public void onMessageEvent(NearbyPlacesReceivedEvent nearbyPlacesreceivedEvent) {
        mRecyclerView.setAdapter(new NearbyPlacesAdapter(getContext(),
                nearbyPlacesreceivedEvent.mNearbyPlacesResponse.results,
                new NearbyPlacesAdapter.AdapterListener() {
                    @Override
                    public void onItemClicked(Location location, String address) {

                        Uri gmmIntentUri = Uri.parse(
                                String.format(Locale.ENGLISH, "geo:0,0?q=%f,%f(%s)",
                                        location.lat, location.lng, address));
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);
                    }
                }));
    }
}
