package com.guidovezzoni.testtide.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.guidovezzoni.testtide.R;

public class NearbyPlacesViewHolder extends RecyclerView.ViewHolder {
    public RelativeLayout mContainer;
    public ImageView mMamIconnImage;
    public TextView mName;
    public TextView mVicinity;
    public TextView mDistance;

    public NearbyPlacesViewHolder(View itemView) {
        super(itemView);

        mContainer = (RelativeLayout) itemView.findViewById(R.id.container);
        mMamIconnImage = (ImageView) itemView.findViewById(R.id.icon);
        mName = (TextView) itemView.findViewById(R.id.bar_name);
        mVicinity = (TextView) itemView.findViewById(R.id.bar_vicinity);
        mDistance = (TextView) itemView.findViewById(R.id.bar_distance);
    }


}
